package qa.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.stereotype.Component;
import qa.enums.Browser;

@Component
public class DriverFactory {
    public static WebDriver get(Browser browser) {
        if (Browser.chrome == browser) {
            new DesiredCapabilities();
            System.setProperty("webdriver.chrome.driver", "C:\\Users\\berdinas\\IdeaProjects\\delta_sprint_3\\delta_producto_tecnico_v1\\src\\test\\resources\\drivers\\win\\chromedriver.exe");
            return new ChromeDriver();
        }

        if (Browser.firefox == browser) {
            new DesiredCapabilities();
            System.setProperty("webdriver.gecko.driver", "C:\\Users\\berdinas\\IdeaProjects\\delta_sprint_3\\delta_producto_tecnico_v1\\src\\test\\resources\\drivers\\win\\geckodriver.exe");
            return new FirefoxDriver();
        }

        throw new IllegalArgumentException("Driver not found for browser: " + browser);
    }
}

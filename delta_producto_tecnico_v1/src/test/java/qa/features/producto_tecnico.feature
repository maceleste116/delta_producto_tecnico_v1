Feature: Administrar Producto Tecnico

  Scenario: Crear producto tecnico con datos requeridos
    Given actuario quiere crear producto tecnico con datos requeridos
    When actuario ingresa datos producto tecnico requeridos
    Then actuario tiene producto tecnico creado

  Scenario: Crear producto tecnico sin datos requeridos
    Given actuario quiere crear producto tecnico sin datos requeridos
    When actuario no ingresa datos producto tecnico requeridos
    Then actuario no tiene producto tecnico creado

  Scenario: Crear producto tecnico con codigo existente
    Given actuario quiere crear producto tecnico con codigo existente
    When actuario ingresa datos producto tecnico con codigo existente
    Then actuario no tiene producto tecnico creado con codigo existente


  Scenario: Crear producto tecnico sin cobertura base
    Given actuario quiere crear producto tecnico sin cobertura base
    When actuario ingresa datos producto tecnico sin cobertura base
    Then actuario no tiene producto tecnico creado sin cobertura base

  Scenario: Modificar producto tecnico existente
    Given actuario quiere modificar producto tecnico existente
    When actuario selecciona producto tecnico a modificar
    Then actuario tiene producto tecnico modificado

  Scenario: Modificar producto tecnico existente sin datos requeridos
    Given actuario quiere modificar producto tecnico existente sin datos requeridos
    When actuario selecciona producto tecnico a modificar sin datos requeridos
    Then actuario no tiene producto tecnico modificado sin datos requeridos

  Scenario: Eliminar producto tecnico existente
    Given actuario quiere eliminar producto tecnico existente
    When actuario selecciona producto tecnico a eliminar
    Then actuario tiene producto tecnico eliminado
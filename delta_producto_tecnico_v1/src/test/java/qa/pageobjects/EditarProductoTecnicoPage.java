package qa.pageobjects;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Getter
public class EditarProductoTecnicoPage extends PageBase {

    @Autowired
    public EditarProductoTecnicoPage(WebDriver driver){
        super(driver);
    }

    //Read only
    //@FindBy(xpath="//input[@id='productos-tecnico-input-codigo']")
    //private WebElement inputEditarCodigo;

    @FindBy(xpath="//input[@id='productos-tecnico-input-descripcion']")
    private WebElement inputEditarDescripcion;

    @FindBy(xpath="//select[@id='productos-tecnico-select-estado']")
    private WebElement selectEditarEstado;

    @FindBy(xpath="//input[@id='productos-tecnico-input-nombre']")
    private WebElement inputEditarNombre;

    @FindBy(xpath="//select[@id='productos-tecnico-select-ramo']")
    private WebElement selectEditarRamo;

    @FindBy(xpath="//input[@id='productos-tecnico-checkbox-1']")
    private WebElement checkBaseFirst;

    @FindBy(xpath="(//input[@id='productos-tecnico-checkbox-2'])[2]")
    private WebElement checkAdicionalFirst;

    @FindBy(xpath="(//input[@id='productos-tecnico-checkbox-1'])[3]")
    private WebElement checkAccesoriaFirst;

    @FindBy(xpath="//button[@id='producto-tecnico-boton-confirmar-producto-tecnico']/span")
    private WebElement buttonConfirmarEditar;

    @FindBy(css=".modal-label")
    private WebElement mensajeEditar;


}

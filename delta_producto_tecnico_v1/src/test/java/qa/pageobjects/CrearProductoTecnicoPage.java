package qa.pageobjects;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Getter
public class CrearProductoTecnicoPage extends PageBase {

    @Autowired
    public CrearProductoTecnicoPage(WebDriver driver){
        super(driver);
    }

    @FindBy(xpath="//input[@id='productos-tecnico-input-codigo']")
    private WebElement inputCrearCodigo;

    @FindBy(xpath="//input[@id='productos-tecnico-input-descripcion']")
    private WebElement inputCrearDescripcion;

    @FindBy(xpath="//select[@id='productos-tecnico-select-estado']")
    private WebElement selectCrearEstado;

    @FindBy(xpath="//input[@id='productos-tecnico-input-nombre']")
    private WebElement inputCrearNombre;

    @FindBy(xpath="//select[@id='productos-tecnico-select-ramo']")
    private WebElement selectCrearRamo;

    @FindBy(xpath="//input[@id='productos-tecnico-checkbox-0']")
    private WebElement checkBaseFirst;

    @FindBy(xpath="(//input[@id='productos-tecnico-checkbox-0'])[2]")
    private WebElement checkAdicionalFirst;

    @FindBy(xpath="(//input[@id='productos-tecnico-checkbox-0'])[3]")
    private WebElement checkAccesoriaFirst;

    @FindBy(id="producto-tecnico-boton-confirmar-producto-tecnico")
    private WebElement buttonCrearConfirmar;

    @FindBy(id="producto-tecnico-boton-cancelar-producto-tecnico")
    private WebElement buttonCrearCancelar;

    @FindBy(css=".modal-label")
    private WebElement mensajeCrear;


}

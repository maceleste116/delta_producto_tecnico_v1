package qa.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HomeProductoTecnicoServices {

    @Autowired
    private WebDriver driver;

    @Autowired
    private WebDriverWait wait;

    @Autowired
    private HomeProductoTecnicoPage homeProductoTecnicoPage;

    public void clickOnMenuPrincipal() {
        this.wait.until(ExpectedConditions.visibilityOf(this.homeProductoTecnicoPage.getMenuPrincipal()));
        this.homeProductoTecnicoPage.getMenuPrincipal().click();
    }

    public void clickOnMenuProductoTecnico() {

        this.homeProductoTecnicoPage.getMenuProductoTecnico().click();
    }

    public void clickOnButtonCrear() {
        this.wait.until(ExpectedConditions.visibilityOf(homeProductoTecnicoPage.getButtonCrear()));
        this.homeProductoTecnicoPage.getButtonCrear().click();
    }

    public void clickOnTablaMostrarAccion() {
        this.wait.until(ExpectedConditions.visibilityOf(this.homeProductoTecnicoPage.getTablaMostrarAccion()));
        this.homeProductoTecnicoPage.getTablaMostrarAccion().click();
    }

    public void clickOnTablaButtonEditar() {
        this.homeProductoTecnicoPage.getTablaButtonEditar().click();
    }

    public void clickOnTablaButtonEliminar() {
        this.wait.until(ExpectedConditions.visibilityOf(homeProductoTecnicoPage.getTablaButtonEliminar()));
        this.homeProductoTecnicoPage.getTablaButtonEliminar().click();
    }

    public String getMensajeEliminar(){
        this.wait.until(ExpectedConditions.visibilityOf(this.homeProductoTecnicoPage.getMensajeEliminar()));
        return this.homeProductoTecnicoPage.getMensajeEliminar().getText();
    }


}

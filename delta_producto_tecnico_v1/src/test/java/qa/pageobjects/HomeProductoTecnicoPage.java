package qa.pageobjects;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Getter
public class HomeProductoTecnicoPage extends PageBase {

    @Autowired
    public HomeProductoTecnicoPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id="nabvar-burguer-menu-boton-desplegar")
    private WebElement menuPrincipal;

    @FindBy(id="nabvar-burguer-menu-boton-productos-tecnicos")
    private WebElement menuProductoTecnico;

    @FindBy(id="productos-tecnicos-tabla-boton-mostrar-acciones-0")
    private WebElement tablaMostrarAccion;

    @FindBy(xpath="//button[@id='productos-tecnicos-boton-crear-producto-tecnico']/span")
    private WebElement buttonCrear;

    @FindBy(id="productos-tecnicos-tabla-boton-editar-0")
    private WebElement tablaButtonEditar;

    @FindBy(id="productos-tecnicos-tabla-boton-eliminar-0")
    private WebElement tablaButtonEliminar;

    @FindBy(css=".modal-label")
    private WebElement mensajeEliminar;


}

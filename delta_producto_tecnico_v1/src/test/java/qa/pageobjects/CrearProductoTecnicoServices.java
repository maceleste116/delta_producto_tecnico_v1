package qa.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CrearProductoTecnicoServices {

    @Autowired
    private WebDriver driver;

    @Autowired
    private WebDriverWait wait;

    @Autowired
    private CrearProductoTecnicoPage crearProductoTecnicoPage;

    public void writeInputCrearCodigo(String codigo){
        this.wait.until(ExpectedConditions.visibilityOf(this.crearProductoTecnicoPage.getInputCrearCodigo()));
        this.crearProductoTecnicoPage.getInputCrearCodigo().sendKeys(codigo);
    }

    public void writeInputCrearDescripcion(String descripcion){
        this.crearProductoTecnicoPage.getInputCrearDescripcion().sendKeys(descripcion);
    }

    public void selectEstado(String estado){
        this.wait.until(ExpectedConditions.visibilityOf(this.crearProductoTecnicoPage.getSelectCrearEstado()));
        new Select(this.crearProductoTecnicoPage.getSelectCrearEstado()).selectByVisibleText(estado);
    }

    public void writeInputCrearNombre(String nombre){
        this.crearProductoTecnicoPage.getInputCrearNombre().sendKeys(nombre);
    }

    public void selectRamo(String ramo){
        new Select(this.crearProductoTecnicoPage.getSelectCrearRamo()).selectByVisibleText(ramo);
    }

    public void checkCoberturaBase(){
        this.wait.until(ExpectedConditions.visibilityOf(this.crearProductoTecnicoPage.getCheckBaseFirst()));
        this.crearProductoTecnicoPage.getCheckBaseFirst().click();
    }

    public void checkCoberturaAdicional(){
        this.wait.until(ExpectedConditions.visibilityOf(this.crearProductoTecnicoPage.getCheckAdicionalFirst()));
        this.crearProductoTecnicoPage.getCheckAdicionalFirst().click();
    }

    public void checkCoberturaAccesoria(){
        this.crearProductoTecnicoPage.getCheckAccesoriaFirst().click();
    }

    public void clickOnButtonConfirmar(){

        this.crearProductoTecnicoPage.getButtonCrearConfirmar().click();
    }

    public void clickOnButtonCancelar() {
        this.crearProductoTecnicoPage.getButtonCrearCancelar().click();
    }

    public String getMensajeCrear(){
        this.wait.until(ExpectedConditions.visibilityOf(this.crearProductoTecnicoPage.getMensajeCrear()));
        return this.crearProductoTecnicoPage.getMensajeCrear().getText();
    }



}

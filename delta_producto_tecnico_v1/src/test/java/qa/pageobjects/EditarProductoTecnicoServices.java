package qa.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EditarProductoTecnicoServices {

    @Autowired
    private WebDriver driver;

    @Autowired
    private WebDriverWait wait;

    @Autowired
    private EditarProductoTecnicoPage editarProductoTecnicoPage;

    //public void writeEditInputCodigo(String codigo){
        //this.editarProductoTecnicoPage.getInputEditarCodigo().clear();
        //this.editarProductoTecnicoPage.getInputEditarCodigo().sendKeys(codigo);
    //}

    public void writeEditInputDescripcion(String descripcion){
        //this.wait.until(ExpectedConditions.visibilityOf(this.editarProductoTecnicoPage.getInputEditarDescripcion()));
        this.editarProductoTecnicoPage.getInputEditarDescripcion().clear();
        this.editarProductoTecnicoPage.getInputEditarDescripcion().sendKeys(descripcion);

    }

    public void selectEditEstado(String estado){
        this.wait.until(ExpectedConditions.visibilityOf(this.editarProductoTecnicoPage.getSelectEditarEstado()));
        new Select(this.editarProductoTecnicoPage.getSelectEditarEstado()).selectByVisibleText(estado);
    }

    public void writeEditInputNombre(String nombre){
        this.editarProductoTecnicoPage.getInputEditarNombre().clear();
        this.editarProductoTecnicoPage.getInputEditarNombre().sendKeys(nombre);

    }

    public void selectEditRamo(String ramo){
        this.wait.until(ExpectedConditions.visibilityOf(this.editarProductoTecnicoPage.getSelectEditarEstado()));
        new Select(this.editarProductoTecnicoPage.getSelectEditarRamo()).selectByVisibleText(ramo);
    }

    public void checkEditarCoberturaBase(){
        this.editarProductoTecnicoPage.getCheckBaseFirst().click();
    }

    public void checkEditarCoberturaAdicional(){

        this.editarProductoTecnicoPage.getCheckAdicionalFirst().click();
    }

    public void checkCoberturaAccesoria(){

        this.editarProductoTecnicoPage.getCheckAccesoriaFirst().click();
    }

    public void clickOnButtonConfirmarEditar(){

        this.editarProductoTecnicoPage.getButtonConfirmarEditar().click();
    }

    public String getMensajeEditar(){
        this.wait.until(ExpectedConditions.visibilityOf(this.editarProductoTecnicoPage.getMensajeEditar()));
        return this.editarProductoTecnicoPage.getMensajeEditar().getText();
    }


}

package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.HomeProductoTecnicoServices;

@Component
public class ValidarEliminarProductoTecnico {

    @Autowired
    private HomeProductoTecnicoServices homeProductoTecnicoServices;

    public boolean validarProductoTecnicoEliminarWithInfoDefault(){
        String mensaje = homeProductoTecnicoServices.getMensajeEliminar();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("eliminada");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;
    }
}

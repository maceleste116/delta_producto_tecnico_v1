package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.HomeProductoTecnicoServices;

@Component
public class MenuProductoTecnico {

    @Autowired
    private HomeProductoTecnicoServices homeProductoTecnicoServices;

    public void IrMenuProductoTecnico(){
        homeProductoTecnicoServices.clickOnMenuPrincipal();
        homeProductoTecnicoServices.clickOnMenuProductoTecnico();

           }
}

package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.models.ProductoTecnico;
import qa.pageobjects.CrearProductoTecnicoServices;
import qa.pageobjects.HomeProductoTecnicoServices;

@Component
public class CrearProductoTecnico {

    @Autowired
    private HomeProductoTecnicoServices homeProductoTecnicoServices;

    @Autowired
    private CrearProductoTecnicoServices crearProductoTecnicoServices;

    public void withInfoRequired(){
        homeProductoTecnicoServices.clickOnButtonCrear();
        crearProductoTecnicoServices.writeInputCrearCodigo("AAAPT5000");
        crearProductoTecnicoServices.writeInputCrearDescripcion("AAAPT 5000");
        crearProductoTecnicoServices.selectEstado("Activo");
        crearProductoTecnicoServices.writeInputCrearNombre("AAAPT 5000");
        crearProductoTecnicoServices.selectRamo("Automotores");
        //Aparecen coberturas
        crearProductoTecnicoServices.checkCoberturaBase();
        crearProductoTecnicoServices.checkCoberturaAdicional();
        crearProductoTecnicoServices.checkCoberturaAccesoria();
        crearProductoTecnicoServices.clickOnButtonConfirmar();
    }

    public void withOutInfoRequired(){
        homeProductoTecnicoServices.clickOnButtonCrear();
        crearProductoTecnicoServices.writeInputCrearCodigo("PT7000");
        crearProductoTecnicoServices.writeInputCrearNombre("");
        crearProductoTecnicoServices.writeInputCrearDescripcion("");
        crearProductoTecnicoServices.selectEstado("Activo");
        crearProductoTecnicoServices.selectRamo("Automotores");
        //Aparecen coberturas
        crearProductoTecnicoServices.checkCoberturaBase();
        crearProductoTecnicoServices.checkCoberturaAdicional();
        crearProductoTecnicoServices.checkCoberturaAccesoria();
        crearProductoTecnicoServices.clickOnButtonConfirmar();
    }

    public void withOutCoberturaBase(){
        homeProductoTecnicoServices.clickOnButtonCrear();
        crearProductoTecnicoServices.writeInputCrearCodigo("PT10000");
        crearProductoTecnicoServices.writeInputCrearDescripcion("PT10000");
        crearProductoTecnicoServices.selectEstado("Activo");
        crearProductoTecnicoServices.writeInputCrearNombre("PT10000");
        crearProductoTecnicoServices.selectRamo("Automotores");
        //Aparecen coberturas
        //crearProductoTecnicoServices.checkCoberturaBase();
        crearProductoTecnicoServices.checkCoberturaAdicional();
        crearProductoTecnicoServices.checkCoberturaAccesoria();
        crearProductoTecnicoServices.clickOnButtonConfirmar();
    }


}

package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.CrearProductoTecnicoServices;
import qa.pageobjects.EditarProductoTecnicoServices;


@Component
public class ValidarCrearProductoTecnico {

    @Autowired
    private CrearProductoTecnicoServices crearProductoTecnicoServices;

    @Autowired
    private EditarProductoTecnicoServices editarProductoTecnicoServices;

    public boolean validarProductoTecnicoWithInfoDefault(){
        String mensaje = crearProductoTecnicoServices.getMensajeCrear();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("correctamente");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;

    }

    public boolean validarProductoTecnicoWithOutInfoDefault(){
        String mensaje = crearProductoTecnicoServices.getMensajeCrear();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("campos");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;

    }

    public boolean validarProductoTecnicoWithCodigoExiste(){
        String mensaje = crearProductoTecnicoServices.getMensajeCrear();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("Ya existe");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;

    }

    public boolean validarProductoTecnicoWithOutCoberturaBase(){
        String mensaje = crearProductoTecnicoServices.getMensajeCrear();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("cobertura base");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;




    }

}

package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.EditarProductoTecnicoServices;

@Component
public class ValidarEditarProductoTecnico {

    @Autowired
    private EditarProductoTecnicoServices editarProductoTecnicoServices;

    public boolean validarProductoTecnicoEditarWithInfoDefault(){
        String mensaje = editarProductoTecnicoServices.getMensajeEditar();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("correctamente");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;
    }

    public boolean validarProductoTecnicoEditarWithOutInfoDefault(){
        String mensaje = editarProductoTecnicoServices.getMensajeEditar();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("campos");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;

    }

}

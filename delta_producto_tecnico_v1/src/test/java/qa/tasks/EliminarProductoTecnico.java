package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.HomeProductoTecnicoServices;

@Component
public class EliminarProductoTecnico {
    @Autowired
    private HomeProductoTecnicoServices homeProductoTecnicoServices;

    public void deleteProductoTecnico() {
        homeProductoTecnicoServices.clickOnTablaMostrarAccion();
        homeProductoTecnicoServices.clickOnTablaButtonEliminar();
        }

    }

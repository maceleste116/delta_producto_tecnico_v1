package qa.tasks;

import org.openqa.selenium.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.testng.annotations.Test;
import qa.models.ProductoTecnico;
import qa.pageobjects.EditarProductoTecnicoServices;
import qa.pageobjects.HomeProductoTecnicoServices;

@Component
public class EditarProductoTecnico {

    @Autowired
    private HomeProductoTecnicoServices homeProductoTecnicoServices;

    @Autowired
    private EditarProductoTecnicoServices editarProductoTecnicoServices;

    public void withInfoRequired() throws InterruptedException {
        homeProductoTecnicoServices.clickOnTablaMostrarAccion();
        homeProductoTecnicoServices.clickOnTablaButtonEditar();
        Thread.sleep(1000);
        editarProductoTecnicoServices.writeEditInputDescripcion("Nuevo Producto Tecnico ");
        Thread.sleep(1000);
        editarProductoTecnicoServices.selectEditEstado("Inactivo");
        editarProductoTecnicoServices.writeEditInputNombre("Nuevo Producto Tecnico");
        //editarProductoTecnicoServices.selectEditRamo("Automotores");
        //Aparecen coberturas
        editarProductoTecnicoServices.checkEditarCoberturaBase();
        editarProductoTecnicoServices.checkEditarCoberturaAdicional();
        editarProductoTecnicoServices.checkCoberturaAccesoria();
        editarProductoTecnicoServices.clickOnButtonConfirmarEditar();

    }

    public void withOutInfoRequired() throws InterruptedException {
        homeProductoTecnicoServices.clickOnTablaMostrarAccion();
        homeProductoTecnicoServices.clickOnTablaButtonEditar();
        Thread.sleep(2000);
        editarProductoTecnicoServices.writeEditInputDescripcion(" \b");
        Thread.sleep(2000);
        editarProductoTecnicoServices.selectEditEstado("Inactivo");
        Thread.sleep(2000);
        editarProductoTecnicoServices.writeEditInputNombre(" \b");
        Thread.sleep(2000);
        //editarProductoTecnicoServices.selectEditRamo("Automotores");
        //Aparecen coberturas
        editarProductoTecnicoServices.checkEditarCoberturaBase();
        Thread.sleep(2000);
        editarProductoTecnicoServices.checkEditarCoberturaAdicional();
        Thread.sleep(2000);
        editarProductoTecnicoServices.checkCoberturaAccesoria();
        Thread.sleep(3000);
        editarProductoTecnicoServices.clickOnButtonConfirmarEditar();
        Thread.sleep(1000);
    }
}

package qa.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductoTecnico {
    private String codigo;
    private String descripcion;
    private String estado;
    private String nombre;
    private String ramo;
    private String coberturaBase;
    private String coberturaAdicional;
    private String coberturaAccesoria;
}

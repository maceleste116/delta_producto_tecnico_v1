package qa.stepdefinitions;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import qa.conf.DriverConfig;
import qa.tasks.*;


@CucumberContextConfiguration
@ContextConfiguration(classes = {DriverConfig.class})
public class ProductoTecnicoStepDefs {

    @Autowired
    private CrearProductoTecnico crearProductoTecnico;

    @Autowired
    private EditarProductoTecnico editarProductoTecnico;

    @Autowired
    private EliminarProductoTecnico eliminarProductoTecnico;

    @Autowired
    private NavigateTo navigateTo;

    @Autowired
    private MenuProductoTecnico menuProductoTecnico;

    @Autowired
    private ValidarCrearProductoTecnico validarCrearProductoTecnico;

    @Autowired
    private ValidarEditarProductoTecnico validarEditarProductoTecnico;

    @Autowired
    private ValidarEliminarProductoTecnico validarEliminarProductoTecnico;

    //CREAR NUEVO PT CON DATOS REQUERIDOS
    @Given("actuario quiere crear producto tecnico con datos requeridos")
    public void actuario_quiere_crear_producto_tecnico_con_datos_requeridos() throws InterruptedException {
       navigateTo.homePage();
        Thread.sleep(1000);
       menuProductoTecnico.IrMenuProductoTecnico();
        Thread.sleep(2000);
    }

    @When("actuario ingresa datos producto tecnico requeridos")
    public void actuario_ingresa_datos_producto_tecnico_requeridos() throws InterruptedException {
        crearProductoTecnico.withInfoRequired();
        Thread.sleep(2000);
    }

    @Then("actuario tiene producto tecnico creado")
    public void actuario_tiene_producto_tecnico_creado() {
        Assert.assertTrue(validarCrearProductoTecnico.validarProductoTecnicoWithInfoDefault());
    }


    //NO CREAR NUEVO PRODUCTO EXISTENTE SIN DATOS REQUERIDOS
    @Given("actuario quiere crear producto tecnico sin datos requeridos")
    public void actuario_quiere_crear_producto_tecnico_sin_datos_requeridos() throws InterruptedException {
        navigateTo.homePage();
        Thread.sleep(1000);
        menuProductoTecnico.IrMenuProductoTecnico();
        Thread.sleep(2000);
    }

    @When("actuario no ingresa datos producto tecnico requeridos")
    public void actuario_no_ingresa_datos_producto_tecnico_requeridos() throws InterruptedException {
        crearProductoTecnico.withOutInfoRequired();
        Thread.sleep(2000);
    }

    @Then("actuario no tiene producto tecnico creado")
    public void actuario_no_tiene_producto_tecnico_creado() {
        Assert.assertTrue(validarCrearProductoTecnico.validarProductoTecnicoWithOutInfoDefault());
    }

    //CREAR PT CON CODIGO EXIXTENTE
    @Given("actuario quiere crear producto tecnico con codigo existente")
    public void actuario_quiere_crear_producto_tecnico_con_codigo_existente() throws InterruptedException {
        navigateTo.homePage();
        Thread.sleep(1000);
        menuProductoTecnico.IrMenuProductoTecnico();
        Thread.sleep(2000);
    }

    @When("actuario ingresa datos producto tecnico con codigo existente")
    public void actuario_ingresa_datos_producto_tecnico_con_codigo_existente() throws InterruptedException {
        crearProductoTecnico.withInfoRequired();
        Thread.sleep(1000);
    }

    @Then("actuario no tiene producto tecnico creado con codigo existente")
    public void actuario_no_tiene_producto_tecnico_creado_con_codigo_existente() {
        Assert.assertTrue(validarCrearProductoTecnico.validarProductoTecnicoWithCodigoExiste());
    }

    //CREAR PRODUCTO TECNICO SIN COBERTURA BASE
    @Given("actuario quiere crear producto tecnico sin cobertura base")
    public void actuario_quiere_crear_producto_tecnico_sin_cobertura_base() throws InterruptedException {
        navigateTo.homePage();
        Thread.sleep(1000);
        menuProductoTecnico.IrMenuProductoTecnico();
        Thread.sleep(1000);
    }

    @When("actuario ingresa datos producto tecnico sin cobertura base")
    public void actuario_ingresa_datos_producto_tecnico_sin_cobertura_base() throws InterruptedException {
        crearProductoTecnico.withOutCoberturaBase();
        Thread.sleep(1000);
    }

    @Then("actuario no tiene producto tecnico creado sin cobertura base")
    public void actuario_no_tiene_producto_tecnico_creado_sin_cobertura_base() {
        Assert.assertTrue(validarCrearProductoTecnico.validarProductoTecnicoWithOutCoberturaBase());

    }

    //MODIFICAR PT EXISTENTE
    @Given("actuario quiere modificar producto tecnico existente")
    public void actuario_quiere_modificar_producto_tecnico_existente() throws InterruptedException {
        navigateTo.homePage();
        Thread.sleep(1000);
        menuProductoTecnico.IrMenuProductoTecnico();
        Thread.sleep(1000);
    }

    @When("actuario selecciona producto tecnico a modificar")
    public void actuario_selecciona_producto_tecnico_a_modificar() throws InterruptedException {
        editarProductoTecnico.withInfoRequired();
        Thread.sleep(1000);
    }

    @Then("actuario tiene producto tecnico modificado")
    public void actuario_tiene_producto_tecnico_modificado() {
        Assert.assertTrue(validarEditarProductoTecnico.validarProductoTecnicoEditarWithInfoDefault());
    }

    //MODIFICAR PRODUCTO TECNICO EXISTENTE SIN DATOS REQUERIDOS
    @Given("actuario quiere modificar producto tecnico existente sin datos requeridos")
    public void actuario_quiere_modificar_producto_tecnico_existente_sin_datos_requeridos() throws InterruptedException {
        navigateTo.homePage();
        Thread.sleep(1000);
        menuProductoTecnico.IrMenuProductoTecnico();
        Thread.sleep(1000);

    }

    @When("actuario selecciona producto tecnico a modificar sin datos requeridos")
    public void actuario_selecciona_producto_tecnico_a_modificar_sin_datos_requeridos() throws InterruptedException {
        editarProductoTecnico.withOutInfoRequired();
        Thread.sleep(1000);
    }

    @Then("actuario no tiene producto tecnico modificado sin datos requeridos")
    public void actuario_no_tiene_producto_tecnico_modificado_sin_datos_requeridos() {
        Assert.assertTrue(validarEditarProductoTecnico.validarProductoTecnicoEditarWithOutInfoDefault());

    }


    //ELIMINAR PRODUCTO TECNICO
    @Given("actuario quiere eliminar producto tecnico existente")
    public void actuario_quiere_eliminar_producto_tecnico_existente() throws InterruptedException {
        navigateTo.homePage();
        Thread.sleep(1000);
        menuProductoTecnico.IrMenuProductoTecnico();
        Thread.sleep(1000);
    }

    @When("actuario selecciona producto tecnico a eliminar")
    public void actuario_selecciona_producto_tecnico_a_eliminar() {
        eliminarProductoTecnico.deleteProductoTecnico();

    }

    @Then("actuario tiene producto tecnico eliminado")
    public void actuario_tiene_producto_tecnico_eliminado() {

        Assert.assertTrue(validarEliminarProductoTecnico.validarProductoTecnicoEliminarWithInfoDefault());


    }

}

